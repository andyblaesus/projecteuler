##Solution to Project Euler problem 3
##Author: Andy Shu Xin
##License: CC0. All rights waived.

def solu1(NUM):
    #Algorithm: Iterate from 3 to sqrt(NUM), check if iterator is both prime and a factor of NUM,
    #if so, update the largest record
    #This algorithm is too slow
    ans = 0
    prime = [2]  #List of prime numbers, useful in examination of future prime numbers
    i = 3
    while i**2 < NUM:  #better than i < math.sqrt(NUM), saves the trouble of importing
        isPrime = True
        for primeNumber in prime:
            if i % primeNumber == 0:
                isPrime = False
                break
        if isPrime:
            prime.append(i)
            if NUM % i ==0:
                ans = i
        i += 1
    return ans

def solu2(NUM):
    #Algorithm:
    #(1)start i from 2,
    #(2)if NUM is divisible by i:
    #   (2.1) record i as the best
    #   (2.2) Divide NUM by i until NUM is not divisible by i
    #(3)i++ And 
    #(4)go to (2)
    #Supposedly, [i] should only need to be prime numbers, but there is
    #a walkaround. No composite [i] would be wrongfully recorded as result,
    #because previous prime numbers already rule out that possiblilty.
    #For example, after loop for i=2 is run, it is imporsible for
    #all multiple of 2 to enter the (2) step.
    #More optimization is obviously possible, albeit unncessary at this point.
    #With pypy, I got the answer within 0.02s.

    n = NUM  #Can skip. Just NUM is supposed to be constant.
    i = 2
    max = 0
    while i**2 < n:
        while NUM % i == 0:
            max = i
            NUM = NUM / i
        i += 1
    return max

def main():
    NUM = 600851475143
    print solu2(NUM)

if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    main()
