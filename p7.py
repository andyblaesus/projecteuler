##Solution to Project Euler problem 7
##Author: Andy Shu Xin
##License: CC0. All rights waived.

REQUIRED_ORDER = 10001
def main():
    max = 10000
    while 1:
        prime = []  #This step can be easily forgotten
        sieve = [2] + [2*i+1 for i in range(1, max/2)]
        #Sieving!
        for i in sieve:
            prime.append(i)
            if len(prime) >= REQUIRED_ORDER:
                return prime[REQUIRED_ORDER - 1]
            for k in sieve:
                if (k % i == 0) and (k != i):
                    sieve.remove(k)
        #END Sieve
        max *= 2  #Enlarge the sieve if answer not found
        print max
    #END WHILE


if __name__ == '__main__':
    #import cProfile
    #cProfile.run('print main()')
    print main()
