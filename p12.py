##Solution to Project Euler problem 12
##Author: Andy Shu Xin
##License: CC0. All rights waived.

MAX = 500

def getTri(seed):
    ans = 0
    for i in range(1, seed+1):
        ans += i
    return ans

def is_prime(i, primes):
    if abs(i % 6) in (0, 2, 3, 4):  #a prime must be like either 6n+1 or 6n+5
        return False
    else:
        ans = True
        for k in primes:
            if i % k == 0:
                ans = False
                break
            #Interestingly, if change the condition to
            #if (k*k <= i) and (i % k == 0): , in an attempt to optimize,
            #the performance is actually worse.
            #An [if k*k > i: break] statement put in front doesn't work either.
        return ans

def get_next_prime(primes):
    i = primes[-1] + 2
    while not is_prime(i, primes):
        i += 2
    return i

def main():
    primes = [2, 3]
    nod = 1  #Number of divisors
    triSeed = 1
    while nod <= MAX:
        n = getTri(triSeed)
        triSeed += 1
        nod = 1  #Number of divisor
        factors = {}
        #expanding prime list to sufficient length
        while primes[-1]*primes[-1] < n:
            primes.append(get_next_prime(primes))
        #All prime factor of n -> factors
        for p in primes:
            if n % p == 0:
                n_copy = n
                factors[p] = 1
                n_copy /= p
                while n_copy % p == 0:
                    n_copy /= p
                    factors[p] += 1

        #Divisors of a number can be constructed by its prime factors.
        #To illustrate: 72 = 3**1 * 5**2. Therefore, 72 has (1+1)*(2+1) = 6 divisors.
        #216 = 3**2 * 5**2. Therefore, 216 has (2+1)*(2+1) = 9 divisors. 
        for factor in factors:
            nod *= factors[factor] + 1
    return n
    #END while

if __name__ == '__main__':
    print main()
