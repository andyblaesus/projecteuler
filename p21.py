##Solution to Project Euler problem 21
##Author: Andy Shu Xin
##License: CC0. All rights waived.
##Bear resemblance to EQ 12, but due to a much smaller scale,
##brute force is easier.

from __future__ import print_function

def get_factors(n):
    ans = [1]
    for i in range(2, n/2+1):
        if n % i == 0:
            ans += [i]
    return ans

def d(n):
    return sum(get_factors(n))

def main():
    amicableList = []
    for i in range(2, 10000):
        di = d(i)
        if i == d(di) and i != di:  #Tricky
            amicableList += [i]
    return sum(amicableList)


if __name__ == '__main__':
    print(main())
