##Solution to Project Euler problem 14
##Author: Andy Shu Xin
##License: CC0. All rights waived.

def getLength(n):
    if n == 1:
        return 1
    else:
        if n % 2 == 0:
            return 1 + getLength(n/2)
        else:
            return 1 + getLength(3*n+1)

def main():
    maxLength = 0
    maxN = 0
    for i in range(1, 1000000):
        if i % 10000 == 0:
            print 'Examinging:', i
        L = getLength(i)
        if L > maxLength:
            maxLength = L
            maxN = i
    return maxN

if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    print main()
