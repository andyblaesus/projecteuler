##Solution to Project Euler problem 15
##Author: Andy Shu Xin
##License: CC0. All rights waived.

from __future__ import print_function

BORDER = 20 

def getRoutes(coordinate, M):
    if coordinate in M:
        return M[coordinate]
    else:
        res =  getRoutes((coordinate[0] + 1, coordinate[1]), M) + \
               getRoutes((coordinate[0], coordinate[1] + 1), M)
        M[coordinate] = res
        return res

def main():
    M = {}
    for i in range(0, BORDER+1):
        M[BORDER,i] = 1
        M[i,BORDER] = 1
    M[BORDER+1, BORDER+1] = 0
    return getRoutes((0,0), M)

if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    print(main())
