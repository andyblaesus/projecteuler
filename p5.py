##Solution to Project Euler problem x
##Author: Andy Shu Xin
##License: CC0. All rights waived.

def solu1(n):
    found = False
    while not found:
        n += 11  #Theoretically, n += 1 also works, it's just too slow.
        found = True
        for i in range(12, 20+1):  #i=2..11 is gauranteed.
            if n % i != 0:
                found = False
    return n

def main():
    n = 2520*11  #Convenient starting point
    print solu1(n)


if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    main()
