##Solution to Project Euler problem 16
##Author: Andy Shu Xin
##License: CC0. All rights waived.

from __future__ import print_function

def main():
    A = 2**1000
    sum = 0
    while A != 0:
        sum += A % 10
        A /= 10
    return sum

if __name__ == '__main__':
    print(main())
