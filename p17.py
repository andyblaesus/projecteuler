##Solution to Project Euler problem 17
##Author: Andy Shu Xin
##License: CC0. All rights waived.

from __future__ import print_function

def main():
    i2L = {
            1:len('one'),
            2:len('two'),
            3:len('three'),
            4:len('four'),
            5:len('five'),
            6:len('six'),
            7:len('seven'),
            8:len('eight'),
            9:len('nine'),
            10:len('ten'),
            11:len('eleven'),
            12:len('twelve'),
            13:len('thirteen'),
            14:len('fourteen'),
            15:len('fifteen'),
            16:len('sixteen'),
            17:len('seventeen'),
            18:len('eighteen'),
            19:len('nineteen'),
            20:len('twenty')
            }

    prefix = {20:len('twenty'),  #TWENTY
              30:len('thirty'),  #THIRTY
              40:len('fourty'),  #FOURTY  #This one is tricky.
              50:len('fifty'),  #FIFTY
              60:len('sixty'),  #SIXTY
              70:len('seventy'),  #SEVENTY
              80:len('eighty'),  #EIGHTY
              90:len('ninety'),  #NINETY
              100:len('hundred'), #HUNDRED
              1000:len('thousand') #THOUSAND
              }
    AND = 3

    for i in range(21, 1000+1):
        if i < 100:
            if i % 10 != 0:
                i2L[i] = prefix[i / 10 * 10] + i2L[i % 10]
            else:
                i2L[i] = prefix[i / 10 * 10]
                #So, 41 is 'fourty-one', instead of 'forty-one'?

        elif (i > 100) and (i != 1000):
            if i % 100 == 0:  #X00
                i2L[i] = i2L[i / 100] + prefix[100]
            else:
                i2L[i] = i2L[i / 100] + prefix[100] + AND + i2L[i % 100]
        elif i == 100:
            i2L[i] = i2L[1] + prefix[100]
        elif i == 1000:
            i2L[i] = i2L[1] + prefix[1000]
    #ENDFOR all integers

    sum = 0
    for key in i2L:
        sum += i2L[key]
        print(key, i2L[key])
    #for x in range(1,100):
        #sum += i2L[x]
        #print(x, i2L[x])
    return sum

if __name__ == '__main__':
    print(main())
