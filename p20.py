##Solution to Project Euler problem 20
##Author: Andy Shu Xin
##License: CC0. All rights waived.

from __future__ import print_function

def main():
    factorial = 1
    for i in range(2, 101):
        factorial *= i

    sum = 0
    while factorial != 0:
        sum += factorial % 10
        factorial /= 10
    return sum

if __name__ == '__main__':
    print(main())
