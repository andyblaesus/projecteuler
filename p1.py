##Solution to Project Euler problem 1
##Author: Andy Shu Xin
##License: CC0. All rights waived.
Problem_Num = 1

def main():
    sum = 0
    for i in range(1, 1000):
        if (i % 3 == 0) or (i % 5 == 0):
            sum += i
    print "\n" * 50
    print "Answer to problem "+str(Problem_Num)+": "+str(sum)

if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    main()
