##Solution to Project Euler problem 6
##Author: Andy Shu Xin
##License: CC0. All rights waived.

def main():
    sum1 = 0
    sum2 = 0

    for i in range(1, 100+1):
        sum1 += i*i
        sum2 += i

    sum2 = sum2 * sum2
    return (sum2 - sum1)

if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    print main()
