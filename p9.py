##Solution to Project Euler problem 9
##Author: Andy Shu Xin
##License: CC0. All rights waived.

def main():
    for a in range(1, 334):
        for b in range(a+1,  501):
            c = 1000 - a - b
            if a*a + b*b == c*c:
                return a*b*c


if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    print main()
