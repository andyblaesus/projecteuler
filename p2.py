##Solution to Project Euler problem 2
##Author: Andy Shu Xin
##License: CC0. All rights waived.
Problem_Num = 2

def prob2():
    FOUR_MILLION = 4*(10**6)
    pre, cur = 1, 1
    sum = 0
    while cur < FOUR_MILLION:
        if cur % 2 == 0:
            sum += cur
        pre, cur = cur, cur + pre
    return sum

def main():
    print "Answer to problem "+str(Problem_Num)+": "+str(prob2())

if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    main()
