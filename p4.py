##Solution to Project Euler problem x
##Author: Andy Shu Xin
##License: CC0. All rights waived.

def main():
    max = 0
    for i in range(100, 1000):
        for j in range(100,1000):
                k = i * j
                if (str(k) == str(k)[::-1]) and (max < k):
                    max = k
    return max

if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    print main()
