##Solution to Project Euler problem 10
##Author: Andy Shu Xin
##License: CC0. All rights waived.

MAX = 2000000

def main():
    prime = [2]
    i = 3
    while i < MAX:
        isPrime = True
        for p in prime:
            if p**2 > i: 
                break
            if i % p == 0:
                isPrime = False
        if isPrime:
            prime.append(i)
        i += 2

    sum = 0
    for p in prime:
        sum += p
    return sum

if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()')
    print main()
